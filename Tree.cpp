/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Arvore.cpp
 * Author: gerson
 * 
 * Created on 1 de Maio de 2018, 11:37
 */
#include <stddef.h>
#include <iostream>
#include <algorithm>
#include <valarray>
#include "Tree.h"

using namespace std;

Tree::Tree() {
    root = NULL;
}

void Tree::TreeBinaryfi(int vector[],int tam){
    for (int i = 0; i < tam; i++) {
        insert(vector[i]);
    }
}

void Tree::insert(int key){
    insert(this->root,key);
}

int Tree::Search(Node *&node, int key) {
    int f = 0;

    if (node == NULL) {
        f = 0;
    } else if (key == node->key) {
        f = 1;
    } else if (key < node->key) {
        if (node->left == NULL) {
            f = 2;
        } else {
            node = node->left;
            f = Search(node, key);
        }
    } else {
        if (node->right == NULL) {
            f = 3;
        } else {
            node = node->right;
            f = Search(node, key);
        }
    }

    return f;
}

bool Tree::insert(Node *&no, int key) {
    bool ok;
    int f;
    Node* pt;
    Node* aux;

    ok = true;
    aux = no;

    f = Search(aux, key); //busca para saber se ja tem ou em qual lado inserir
    if (f == 1) {
        ok = false;
    } else {
        pt = new Node(key);
        pt->left = NULL;
        pt->right = NULL;

        if (f == 0) {
            root = pt;
        } else if (f == 2) {
            aux->left = pt;
        } else {
            aux->right = pt;
        }
    }

    return ok;
}

int Tree::getSuces(Node *&node) {
    if (node == NULL) {
        return -1;
    }
    if (node->left != NULL) {
        Node* left = node->left;
        return getSuces(left);
    }
    return node->key;
}

void Tree::remove(int key) {
    remove(key,this->root);
}


Node* Tree::remove(int key,Node *&node) {

    if (node == NULL)
        return NULL;

    if (key < node->key)
        node->left = this->remove(key, node->left);

    else if (key > node->key)
        node->right = this->remove(key, node->right);

    else {

        if (node->left == NULL && node->right == NULL) {
            node = NULL;
            delete(node);
        } else if (node->left == NULL) {
            Node* temp = node;
            node = node->right;
            delete temp;

        } else if (node->right == NULL) {
            Node* temp = node;
            node = node->left;
            delete temp;

        } else {
            int temp = getSuces(node->right);
            node->key = temp;
            node->right = this->remove(temp, node->right);
        }
    }

    return node;
}

Node* Tree::getRoot() {
    return root;
}

void Tree::setRoot(Node* node) {
    root = node;
}

void Tree::preOrdem(){
    preOrdem(this->root);
}

void Tree::Clear() {
    delete root;
    this->root = NULL;
}


void Tree::preOrdem(Node* node) {
    if (node != NULL) {

        cout << node->key << " ";
        preOrdem(node->left);
        preOrdem(node->right);
    }
}