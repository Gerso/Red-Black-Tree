/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TreeNode.h
 * Author: gerson
 *
 * Created on 26 de Maio de 2018, 18:59
 */

#ifndef TREENODE_H
#define TREENODE_H

class Node {
public:
    /**
     * Default constructor
     */
    Node();

    /**
     * Constructs a new node from a key
     * @param key
     */
    Node(int key);

    /**
     * Default destructor
     */
    virtual ~Node();

    // key associated with the node
    int key;

    // Balancing the node
    int balance;

    // left subtree
    Node* left;

    // right subtree
    Node* right;
};

#endif /* TREENODE_H */

