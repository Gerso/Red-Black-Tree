/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Arvore.h
 * Author: gerson
 *
 * Created on 1 de Maio de 2018, 11:37
 */

#ifndef ARVORE_H
#define ARVORE_H
#include "Node.h"

class Tree {
public:
    Tree();
    
    void TreeBinaryfi(int vector[],int tam);
    /**
     * method to check if the element already exists or the place where it should be inserted
     * @param node
     * @param key
     * @return 0 if tree is null, 1 if exist, 2 if is to insert on the left ou 3 if is to insert on the right
     */
    int Search(Node *&node, int key);
    
    void insert(int key);
    
    /**
     * method to pick up the successor from the one you wish to remove
     * @param node
     * @return key
     */
    int getSuces(Node *&node);

    void remove(int key);
    /**
     * method to remove a from the tree
     * @param no : tree root 
     * @param key
     * @return 
     */
    Node* remove(int key,Node *&node);

    /**
     * method to return to root
     * @return root 
     */
    Node* getRoot();

    void setRoot(Node* node);

    void preOrdem();
    
    void Clear();

    Node* root;
private:
    /**
     * method to traverse the tree in preOrder
     * @param no
     */
    void preOrdem(Node* no);

    /**
     * method to insert a new element into the tree
     * @param node
     * @param key
     * @return true or false 
     */
    bool insert(Node *&node, int key);

};

#endif /* ARVORE_H */