/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TreeAVL.cpp
 * Author: gerson
 * 
 * Created on 26 de Maio de 2018, 19:02
 */

#include <stddef.h>
#include <iostream>
#include "TreeAVL.h"
#include "Tree.h"

TreeAVL::TreeAVL() {
    this->count = 0;
    this->root = NULL;
}

TreeAVL::~TreeAVL() {
}

void TreeAVL::Clear() {
    delete this->root;
    this->root = NULL;
    this->count = 0;
}

void TreeAVL::AVLifi(int vector[], int tam) {
    for (int i = 0; i < tam; i++) {
        Insert(vector[i]);
    }
}

void TreeAVL::Insert(int key) {

    bool h = false;

    this->InsertAVL(key, this->root, h);
}

void TreeAVL::InsertAVL(int key, Node* &pA, bool& h) {

    Node* pB = NULL;
    Node* pC = NULL;

    if (pA == NULL) {

        pA = new Node(key);
        this->count++;
        h = true;

    } else if (pA->key == key)
        return;

    else if (key < pA->key) {

        this->InsertAVL(key, pA->left, h);

        if (h) {

            switch (pA->balance) {

                case -1:
                    pA->balance = 0;
                    h = false;
                    break;

                case 0:
                    pA->balance = +1;
                    break;

                case +1:
                    pB = pA->left;

                    if (pB->balance == +1) // rotação LL
                        this->RotateLL(pA, pB);
                    else // rotação LR
                        this->RotateLR(pA, pB, pC);

                    pA->balance = 0;
                    h = false;

            } // end switch

        }

    } else if (key > pA->key) {

        this->InsertAVL(key, pA->right, h);

        if (h) {

            switch (pA->balance) {

                case +1:
                    pA->balance = 0;
                    h = false;
                    break;

                case 0:
                    pA->balance = -1;
                    break;

                case -1:
                    pB = pA->right;

                    if (pB->balance == -1) // rotação RR
                        this->RotateRR(pA, pB);

                    else // rotação RL
                        this->RotateRL(pA, pB, pC);

                    pA->balance = 0;
                    h = false;

            } // end switch

        }
    }
}

Node* TreeAVL::Search(int key) {

    Node* current = this->root;

    while (current != NULL) {

        if (key < current->key) {
            current = current->left; //search on left
        } else if (key > current->key) {
            current = current->right; //search on the right
        } else {//found
            return current;
        }
    }
    return NULL; // not found
}

bool TreeAVL::Remove(int key) {

    bool h = false;
    if ((this->RemoveAVL(key, this->root, h))) {
        this->count--;
        return true;
    } else
        return false;
}

bool TreeAVL::RemoveAVL(int key, Node* &p, bool &h) {

    Node* q = NULL;

    if (p == NULL)
        return false;

    if (key < p->key) {

        this->RemoveAVL(key, p->left, h);
        if (h)
            this->BalanceL(p, h);

    } else {

        if (key > p->key) {

            this->RemoveAVL(key, p->right, h);
            if (h)
                this->BalanceR(p, h);

        } else {

            q = p;

            if (q->right == NULL) {
                p = q->left;
                h = true;
            } else {

                if (q->left == NULL) {
                    p = q->right;
                    h = true;
                } else {

                    this->DelMin(q, q->right, h);
                    if (h)
                        this->BalanceR(p, h);
                }

                q = NULL;
            }
        }

    }

    return true;
}

void TreeAVL::DelMin(Node*& q, Node*& r, bool &h) {

    if (r->left != NULL) {

        this->DelMin(q, r->left, h);
        if (h)
            this->BalanceL(r, h);

    } else {
        q->key = r->key;
        q = r;
        r = r->right;
        h = true;
    }
}

int TreeAVL::Size() {
    return (this->count);
}

void TreeAVL::PrintAVLTree() {
    if (this->root != NULL)
        this->PreOrderAVL(this->root);
    else
        std::cout << "Empty Tree" << std::endl;
}

void TreeAVL::PreOrderAVL(Node* node) {

    if (node != NULL) {
        std::cout << node->key << " ";
        this->PreOrderAVL(node->left);
        this->PreOrderAVL(node->right);
    }
}

void TreeAVL::RotateLL(Node* &pA, Node* &pB) {
    pA->left = pB->right;
    pB->right = pA;
    pA->balance = 0;
    pA = pB;
}

void TreeAVL::RotateLR(Node* &pA, Node* &pB, Node* &pC) {
    pC = pB->right;
    pB->right = pC->left;
    pC->left = pB;
    pA->left = pC->right;
    pC->right = pA;

    if (pC->balance == +1)
        pA->balance = -1;
    else
        pA->balance = 0;

    if (pC->balance == -1)
        pB->balance = +1;
    else
        pB->balance = 0;
       
    pA = pC;
    pC->balance = 0;
}

void TreeAVL::RotateRR(Node* &pA, Node* &pB) {
    pA->right = pB->left;
    pB->left = pA;
    pA->balance = 0;
    pA = pB;
}

void TreeAVL::RotateRL(Node* &pA, Node* &pB, Node* &pC) {
    pC = pB->left;
    pB->left = pC->right;
    pC->right = pB;
    pA->right = pC->left;
    pC->left = pA;

    if (pC->balance == -1)
        pA->balance = +1;
    else
        pA->balance = 0;

    if (pC->balance == +1)
        pB->balance = -1;
    else
        pB->balance = 0;

    pA = pC;
    pC->balance = 0;
}

void TreeAVL::BalanceL(Node* &pA, bool &h) {

    Node* pB = NULL;
    Node* pC = NULL;
    int balB, balC;

    switch (pA->balance) {

        case +1:
            pA->balance = 0;
            break;

        case 0:
            pA->balance = -1;
            h = false;
            break;

        case -1:
            pB = pA->right;
            balB = pB->balance;

            if (balB <= 0) { // Rotação RR
                //this->RotateRR(pA, pB);
                pA->right = pB->left;
                pB->left = pA;
                if (balB == 0) {
                    pA->balance = -1;
                    pB->balance = +1;
                    h = false;
                } else {
                    pA->balance = 0;
                    pB->balance = 0;
                }
                pA = pB;

            } else { // Rotação RL
                //this->RotateRL(pA, pB, pC);
                pC = pB->left;
                balC = pC->balance;
                pB->left = pC->right;
                pC->right = pB;
                pA->right = pC->left;
                pC->left = pA;

                if (balC == -1)
                    pA->balance = +1;
                else
                    pA->balance = 0;
                if (balC == +1)
                    pB->balance = -1;
                else
                    pB->balance = 0;

                pA = pC;
                pC->balance = 0;
            }

    } // end switch

}

void TreeAVL::BalanceR(Node*& pA, bool& h) {

    Node* pB;
    Node* pC;
    int balB, balC;

    // Subárvore direita encolheu
    switch (pA->balance) {

        case -1:
            pA->balance = 0;
            break;

        case 0:
            pA->balance = +1;
            h = false;
            break;

        case +1:
            pB = pA->left;
            balB = pB->balance;

            if (balB >= 0) { // rotação LL

                pA->left = pB->right;
                pB->right = pA;

                if (balB == 0) {
                    pA->balance = +1;
                    pB->balance = -1;
                    h = false;
                } else {
                    pA->balance = 0;
                    pB->balance = 0;
                }
                pA = pB;
            } else { // rotação LR

                pC = pB->right;
                balC = pC->balance;
                pB->right = pC->left;
                pC->left = pB;
                pA->left = pC->right;
                pC->right = pA;

                if (balC == +1)
                    pA->balance = -1;
                else
                    pA->balance = 0;

                if (balC == -1)
                    pB->balance = +1;
                else
                    pB->balance = 0;

                pA = pC;
                pC->balance = 0;
            }
    } // end switch
}