/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: gerson
 *
 * Created on 26 de Maio de 2018, 18:57
 */

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>

#include "TreeAVL.h"
#include "Tree.h"

using namespace std;

int const TAM10 = 10;
int const TAM100 = 100;
int const TAM500 = 500;
int const TAM1000 = 1000;

TreeAVL* treAVL;
Tree* TreeB;

int* sortelementsToInsert(int tam) {
    int* elementsToInsert = new int[tam];
    bool status;

    srand(time(NULL));

    for (int i = 0; i < tam; ++i) {

        do {
            elementsToInsert[i] = (rand() % ((tam - 1) - 0 + 1) + 1);
            status = true;

            for (int j = 0; (j < i) && (status == true); ++j)
                if (elementsToInsert[i] == elementsToInsert[j])
                    status = false;

        } while (status == false);
    }
    return elementsToInsert;
}

int* sortIndexesToRemove(int tam) {
    int* indexesToRemove = new int[tam];
    bool status;

    srand(time(NULL));

    for (int i = 0; i < tam; i++) {

        do {
            indexesToRemove[i] = (rand() % ((tam - 1) - 0 + 1) + 0);
            status = true;

            for (int j = 0; (j < i) && (status == true); ++j)
                if (indexesToRemove[i] == indexesToRemove[j])
                    status = false;
        } while (status == false);
    }
    return indexesToRemove;
}

double calculateRemoveAVL(int tam, int* elementsToInsert, int* indexesToRemove) {
    double time;
    clock_t Ticks[2];
    
    Ticks[0] = clock();
    for (int i = 0; i < tam; i++){
       // cout << "removendo o " << elementsToInsert[indexesToRemove[i]] << " "<< i <<"\n";
        //treAVL->PrintAVLTree();
        treAVL->Remove(elementsToInsert[indexesToRemove[i]]);
    }
    Ticks[1] = clock();

    time = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;

    return time;
}

double calculateRemoveSearch(int tam, int* elementsToInsert, int* indexesToRemove) {
    double time;
    clock_t Ticks[2];

    Ticks[0] = clock();
    for (int i = 0; i < tam; i++) {
        TreeB->remove(elementsToInsert[indexesToRemove[i]]);
    }
    Ticks[1] = clock();

    time = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;
    return time;
}

double calculateInsertAVL(int tam, int* elementsToInsert) {
    double time;
    clock_t Ticks[2];
    Ticks[0] = clock();
    for (int i = 0; i < tam; i++) {
        treAVL->Insert(elementsToInsert[i]);
    }
    Ticks[1] = clock();

    time = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;
    return time;
    // printf("Tempo gasto: %g ms.\n", time);
    //-------------------------------------------------
}

double calculateInsertSearch(int tam, int* elementsToInsert) {
    double time;
    clock_t Ticks[2];

    Ticks[0] = clock();

    for (int i = 0; i < tam; i++) {
        TreeB->insert(elementsToInsert[i]);
    }

    Ticks[1] = clock();

    time = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;
    //printf("Tempo gasto: %g ms.\n", time);
    return time;
}

/*----------------------------------------*/

double* ALV(int tam, int* elementsToInsert, int* indexesToRemove) {
    double* timeInsert = new double[20];
    double* timeRemove = new double[20];
    // position 0 insert time, position 1 remove time, position 2 variance insertion and position 3 variance to remove
    double* timeAndVariance = new double[4]; 
    double mediaInsert = 0;
    double mediaRemove = 0;
    
    for (int i = 0; i < 20; i++) {
        timeInsert[i] += calculateInsertAVL(tam, elementsToInsert);
        timeRemove[i] += calculateRemoveAVL(tam, elementsToInsert, indexesToRemove);
    }
    
    for (int i = 0; i < 20; i++) {
        mediaInsert += timeInsert[i];
        mediaRemove += timeRemove[i];
    }

    mediaInsert /= 20;
    mediaRemove /= 20;
    
    timeAndVariance[0] = mediaInsert;
    timeAndVariance[1] = mediaRemove;
    
    for (int i = 0; i < 20; i++) {
        timeAndVariance[2] += (timeInsert[i]-mediaInsert)*(timeInsert[i]-mediaInsert);
        timeAndVariance[3] += (timeRemove[i]-mediaRemove)*(timeRemove[i]-mediaRemove);
    }
    
    timeAndVariance[2] /= 20;
    timeAndVariance[3] /= 20;
    
    return timeAndVariance;
}

double* Search(int tam, int* elementsToInsert, int* indexesToRemove) {
    double* timeInsert = new double[20];
    double* timeRemove = new double[20];
    // position 0 insert time, position 1 remove time, position 2 variance insertion and position 3 variance to remove
    double* timeAndVariance = new double[4]; 
    double mediaInsert = 0;
    double mediaRemove = 0;
    
    for (int i = 0; i < 20; i++) {
        timeInsert[i] = calculateInsertSearch(tam, elementsToInsert);
        timeRemove[i] = calculateRemoveSearch(tam, elementsToInsert, indexesToRemove);
    }
    
    for (int i = 0; i < 20; i++) {
        mediaInsert += timeInsert[i];
        mediaRemove += timeRemove[i];
    }

    mediaInsert /= 20;
    mediaRemove /= 20;
    
    timeAndVariance[0] = mediaInsert;
    timeAndVariance[1] = mediaRemove;
    
    for (int i = 0; i < 20; i++) {
        timeAndVariance[2] += (timeInsert[i]-mediaInsert)*(timeInsert[i]-mediaInsert);
        timeAndVariance[3] += (timeRemove[i]-mediaRemove)*(timeRemove[i]-mediaRemove);
    }
    
    timeAndVariance[2] /= 20;// decrement -1
    timeAndVariance[3] /= 20;// decrement -1
    
    return timeAndVariance;
}

/*
 * 
 */
int main() {
    //treAVL = new TreeAVL();
    treAVL = new TreeAVL();
    TreeB = new Tree();
    int n, option;
    char exit;
    Node* search = NULL;
    double* timeAVL;
    double* timeSearch;
    int* elementsToInsert;
    int* indexesToRemove;

    elementsToInsert = sortelementsToInsert(TAM10);
    indexesToRemove = sortIndexesToRemove(TAM10);

    timeAVL = ALV(TAM10, elementsToInsert, indexesToRemove);
    timeSearch = Search(TAM10, elementsToInsert, indexesToRemove);
    //insert
    cout << "Insertion time with 10 elements in AVL is : " << (timeAVL[0]) << endl;
    cout << "Insertion time with 10 elements in Tree Search is : " << (timeSearch[0]) << endl;
    //remove
    cout << "Remove time with 10 elements in AVL is : " << (timeAVL[1]) << endl;
    cout << "Remove time with 10 elements in Tree Search is : " << (timeSearch[1]) << endl;
    //variance insert
    cout << "Insertion variance with 10 elements in tree search: " << (timeSearch[2]) << endl;
    cout << "Insertion variance with 10 elements in AVL: " << (timeAVL[3]) << endl;
    //variance remove
    cout << "Remove variance with 10 elements in tree search: " << (timeSearch[3]) << endl;
    cout << "Remove variance with 10 elements in AVL: " << (timeAVL[3]) << endl;
    
    //--------------------------------Calculation with 100--------------------------------------------//
    elementsToInsert = sortelementsToInsert(TAM100);
    indexesToRemove = sortIndexesToRemove(TAM100);
    timeAVL = ALV(TAM100, elementsToInsert, indexesToRemove);
    timeSearch = Search(TAM100, elementsToInsert, indexesToRemove);
    //insert
    cout << "\nInsertion time with 100 elements in AVL is : " << (timeAVL[0]) << endl;
    cout << "Insertion time with 100 elements in Tree Search is : " << (timeSearch[0]) << endl;
    //remove
    cout << "Remove time with 100 elements in AVL is : " << (timeAVL[1]) << endl;
    cout << "Remove time with 100 elements in Tree Search is : " << (timeSearch[1]) << endl;
    //variance insert
    cout << "Insertion variance with 100 elements in tree search: " << (timeSearch[2]) << endl;
    cout << "Insertion variance with 100 elements in AVL: " << (timeAVL[3]) << endl;
    //variance remove
    cout << "Remove variance with 100 elements in tree search: " << (timeSearch[3]) << endl;
    cout << "Remove variance with 100 elements in AVL: " << (timeAVL[3]) << endl;
    
    //--------------------------------Calculation with 500--------------------------------------------//
    elementsToInsert = sortelementsToInsert(TAM500);
    indexesToRemove = sortIndexesToRemove(TAM500);
    timeAVL = ALV(TAM500, elementsToInsert, indexesToRemove);
    timeSearch = Search(TAM500, elementsToInsert, indexesToRemove);
    //insert
    cout << "\nInsertion time with 500 elements in AVL is : " << (timeAVL[0]) << endl;
    cout << "Insertion time with 500 elements in Tree Search is : " << (timeSearch[0]) << endl;
    //remove
    cout << "Remove time with 500 elements in AVL is : " << (timeAVL[1]) << endl;
    cout << "Remove time with 500 elements in Tree Search is : " << (timeSearch[1]) << endl;
    //variance insert
    cout << "Insertion variance with 500 elements in tree search: " << (timeSearch[2]) << endl;
    cout << "Insertion variance with 500 elements in AVL: " << (timeAVL[3]) << endl;
    //variance remove
    cout << "Remove variance with 500 elements in tree search: " << (timeSearch[3]) << endl;
    cout << "Remove variance with 500 elements in AVL: " << (timeAVL[3]) << endl;
    
    //--------------------------------Calculation with 1000--------------------------------------------//
    elementsToInsert = sortelementsToInsert(TAM1000);
    indexesToRemove = sortIndexesToRemove(TAM1000);
    timeAVL = ALV(TAM1000, elementsToInsert, indexesToRemove);
    timeSearch = Search(TAM1000, elementsToInsert, indexesToRemove);
    //insert
    cout << "\nInsertion time with 1000 elements in AVL is : " << (timeAVL[0]) << endl;
    cout << "Insertion time with 1000 elements in Tree Search is : " << (timeSearch[0]) << endl;
    //remove
    cout << "Remove time with 1000 elements in AVL is : " << (timeAVL[1]) << endl;
    cout << "Remove time with 1000 elements in Tree Search is : " << (timeSearch[1]) << endl;
    //variance insert
    cout << "Insertion variance with 1000 elements in tree search: " << (timeSearch[2]) << endl;
    cout << "Insertion variance with 1000 elements in AVL: " << (timeAVL[3]) << endl;
    //variance remove
    cout << "Remove variance with 1000 elements in tree search: " << (timeSearch[3]) << endl;
    cout << "Remove variance with 1000 elements in AVL: " << (timeAVL[3]) << endl;
    
    elementsToInsert = sortelementsToInsert(TAM10);
    calculateInsertAVL(TAM10,elementsToInsert);
    
    while (true) {

        cout << "\n_________________________________\n";
        cout << "|----------- Tree AVL------------|\n";
        cout << "_________________________________\n";
        cout << "|---------- 1 - Insert ----------|\n";
        cout << "|---------- 2 - Print  ----------|\n";
        cout << "|---------- 3 - Delete ----------|\n";
        cout << "|---------- 4 - Size   ----------|\n";
        cout << "|---------- 5 - Search ----------|\n";
        cout << "|---------- 6 - Exit   ----------|\n";
        cout << "|________________________________|\n";
        cin >> option;

        switch (option) {
            case 1:
                cout << "Enter a value to insert:\n";
                cin >> n;
                treAVL->Insert(n);
                break;
            case 2:
                cout << "\n----------- AVL -----------------\n";
                cout << "Print Tree in Pre Order\n";
                treAVL->PrintAVLTree();
                cout << "\n---------------------------------\n";
                cout << "----------- TreeB ---------------\n";
                cout << "Print Tree in Pre Order\n";
                TreeB->preOrdem();
                break;
            case 3:
                cout << "Enter a value to delete: \n";
                cin >> n;
                TreeB->remove(n);
                treAVL->Remove(n);
                break;
            case 4:
                n = treAVL->Size();
                cout << "The Size AVL is: " << n;
                cout << endl;
                break;
            case 5:
                cout << "Enter a value to search: \n";
                cin >> n;
                search = treAVL->Search(n);
                if (search != NULL) {
                    cout << "Value found on the address :" << search << endl;
                } else {
                    cout << "Value not found\n";
                }
                break;
            case 6:
                cout << "Exit (y/n)\n";
                cin >> exit;
                if (exit == 'y' || exit == 'Y') {
                    return 0;
                }
                break;
            default:
                cout << "Please. Choose a valid value!\n";
        }

        cout << "\nContinue (y/n)\n";
        cin >> exit;
        if (exit == 'n' || exit == 'N') {
            return 0;
        }
        system("clear");
    }

    delete elementsToInsert;
    treAVL->~TreeAVL();
    delete TreeB;
    delete indexesToRemove;

    return 0;
}
